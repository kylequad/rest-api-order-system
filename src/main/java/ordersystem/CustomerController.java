package ordersystem;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for REST API endpoints for Customers
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class CustomerController {

	private static final AtomicLong counter = Database.getCustomerCounter();
    private static Map<Long, Customer> customerDb = Database.getCustomerDb();
  
    /**
     * Create a new customer in the database
     * @param customer customer with first and last names
     * @return the customer number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/customers/new")
    public ResponseEntity<Long> addNewCustomer(@RequestBody Customer customer) {
    	customer.setNumber(counter.incrementAndGet());
    	customerDb.put(customer.getNumber(), customer);
    	return new ResponseEntity<>(customer.getNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get a customer from the database
     * @param number the customer number
     * @return the customer if in the database, not found if not
     */
    @GetMapping("/customers/{number}")
    public ResponseEntity<Object> getCustomerByNumber(@PathVariable long number) {
    	if (customerDb.containsKey(number)) {
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    	}
    } 
    
    /**
     * Get an array of customer objects that includes all customers in the database
     * @return The list of customers if the database has any, a message stating none are in the database if not.
     */
   @GetMapping("/customers/") 
   public ResponseEntity<Object> getCustomerArray(){
	   if (customerDb.size() > 0) {
		   return new ResponseEntity<>(customerDb.values(), HttpStatus.OK);
	   } else {
		   return new ResponseEntity<>("No Customers are in the database", HttpStatus.NOT_FOUND);
	   }
   }
   
   /**
    * Changes the first and last name of an existing customer with a known customer number
    * @param customerNumber The customer number
    * @param customer The new customer with a new first and last name
    * @return Message if the customer was found and updated successfully or the customer does not exist. 
    */
   @PostMapping("/customers/{customerNumber}")
   public ResponseEntity<Object> changeCustomerName(@PathVariable long customerNumber, @RequestBody Customer customer){
	   if(customerDb.containsKey(customerNumber)) {
		   customer.setNumber(customerNumber);
		   customerDb.replace(customerNumber, customer);
		   return new ResponseEntity<>("Customer successfully updated.", HttpStatus.OK);
	   } else {
		   return new ResponseEntity<>("Customer does not exist.", HttpStatus.NOT_FOUND);
	   }
   }
   
   /**
    * Gets a customer's number given their first and last name
    * @param firstName The Customer's first name
    * @param lastName The Customer's last name
    * @return The customer's number if found, not found message if not. 
    */
   @GetMapping("/customers/{firstName}/{lastName}")
   public ResponseEntity<Object> getCustomerByName(@PathVariable String firstName, @PathVariable String lastName){
	   for(Map.Entry<Long, Customer> entry : customerDb.entrySet()) {
		   if (entry.getValue().getFirstName().equals(firstName) && entry.getValue().getLastName().equals(lastName)){
			   return new ResponseEntity<>(entry.getKey(), HttpStatus.OK);
		   }
	   } 
	   return new ResponseEntity<>("Customer does not exist.", HttpStatus.NOT_FOUND);
   }
       
}